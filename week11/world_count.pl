use strict;
use warnings;

my ($file) = @ARGV;
open FH, $file;
my @lines = <FH>;
close FH;

my %count = ();
foreach my $r (@lines){
    chomp $r;
    
    foreach my $word (split " ", $r) {
        #print "$word\n";
        if (not exists $count{$word}) {
        # initialize the count for a new word
            $count{$word} = 1;
        }
        else {
        # update the count for an existing word     
            $count{$word}++;
        }
    
    }

}
for my $key (sort keys %count) {
    print "$key $count{$key}\n";
}
